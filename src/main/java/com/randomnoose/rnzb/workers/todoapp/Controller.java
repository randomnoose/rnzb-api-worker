package com.randomnoose.rnzb.workers.todoapp;

import com.randomnoose.rnzb.workers.model.*;
import com.randomnoose.rnzb.workers.providers.NearestLocationProvider;
import com.randomnoose.rnzb.workers.providers.NearestLocationProviderResponse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@RestController
@Validated
public class Controller {

  private static final Logger LOGGER = LogManager.getLogger(Controller.class);
  private static final String ALL_PROVIDERS_FAILED_MESSAGE = "There was a problem with providers API. Please try later";

  @Value("${rnzb.worker.uniquenessDistance}")
  private int uniquenessDistanceThreshold = 10;
  private NearestLocationProvider overpassProvider;
  private NearestLocationProvider googleMapsProvider;

  @GetMapping(value = "/nearestLocation", params = {"lat", "lng", "types"})
  public ResponseEntity<Object> nearestLocation(@RequestParam(value = "lat") double lat,
                                                @RequestParam(value = "lng") double lng,
                                                @RequestParam(value = "types") String types) throws ExecutionException, InterruptedException {
    List<LocationType> locationTypes = LocationType.fromString(types);
    QueryData queryData = getQueryData(lat, lng, locationTypes);

    List<CompletableFuture<NearestLocationProviderResponse>> futureResponses = Stream.of(overpassProvider, googleMapsProvider)
        .map(provider -> provider.get(queryData))
        .flatMap(List::stream)
        .collect(toList());


    CompletableFuture.allOf(futureResponses.toArray(new CompletableFuture[futureResponses.size()]));

    List<NearestLocationProviderResponse> responses = new LinkedList<>();
    for (CompletableFuture<NearestLocationProviderResponse> futureResponse : futureResponses) {
      responses.add(futureResponse.get());
    }

    boolean isAnyResult = responses.stream()
        .anyMatch(NearestLocationProviderResponse::isSuccessful);
    if (!isAnyResult) {
      return ResponseEntity.ok(new NearestLocationsResponse()
          .withErrorMessages(singletonList(ALL_PROVIDERS_FAILED_MESSAGE)));
    }

    List<Locations> locationsTypes = new ArrayList<>();
    responses.stream()
        .map(NearestLocationProviderResponse::getLocations)
        .filter(Objects::nonNull)
        .flatMap(List::stream)
        .filter(Objects::nonNull)
        .filter(location -> location.getLocationType() != null)
        .collect(groupingBy(Location::getLocationType))
        .forEach((type, locationsOfType) -> locationsTypes.add(new Locations()
            .withType(type)
            .withLocations(mergeDuplicates(locationsOfType))));

    return ResponseEntity.ok(new NearestLocationsResponse()
        .withLocationsTypes(locationsTypes));
  }

  private List<Location> mergeDuplicates(List<Location> locationsOfType) {
    if (locationsOfType.isEmpty()) {
      return Collections.emptyList();
    }

    List<Location> mergedLocations = new LinkedList<>();
    for (int i = 0; i < locationsOfType.size(); i++) {
      boolean isUnique = true;
      Location firstLocation = locationsOfType.get(i);
      for (int j = i + 1; j < locationsOfType.size(); j++) {
        Location secondLocation = locationsOfType.get(j);
        double distance = GeoHelpers.distanceNoHeight(firstLocation.getAddress().getLatitude(), secondLocation.getAddress().getLatitude(),
            firstLocation.getAddress().getLongitude(), secondLocation.getAddress().getLongitude());
        if (distance < uniquenessDistanceThreshold) {
          isUnique = false;
        }
      }
      // Last will be unique, because it wasn't compared with anything as the firstLocation
      if (isUnique) {
        mergedLocations.add(firstLocation);
      }
    }

    return mergedLocations;
  }

  private QueryData getQueryData(double lat, double lng, List<LocationType> locationTypes) {
    return QueryData.of(lat, lng, locationTypes);
  }

  @Autowired
  public void setOverpassProvider(@Qualifier("overpassProvider") NearestLocationProvider overpassProvider) {
    this.overpassProvider = overpassProvider;
  }

  @Autowired
  public void setGoogleMapsProvider(@Qualifier("googleMapsProvider") NearestLocationProvider googleMapsProvider) {
    this.googleMapsProvider = googleMapsProvider;
  }
}

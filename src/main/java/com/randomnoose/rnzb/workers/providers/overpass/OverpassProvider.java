package com.randomnoose.rnzb.workers.providers.overpass;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.randomnoose.rnzb.workers.model.Address;
import com.randomnoose.rnzb.workers.model.Location;
import com.randomnoose.rnzb.workers.model.LocationType;
import com.randomnoose.rnzb.workers.model.QueryData;
import com.randomnoose.rnzb.workers.providers.NearestLocationProvider;
import com.randomnoose.rnzb.workers.providers.NearestLocationProviderResponse;
import com.randomnoose.rnzb.workers.providers.ProviderException;
import com.randomnoose.rnzb.workers.providers.overpass.model.Element;
import com.randomnoose.rnzb.workers.providers.overpass.model.OverpassQueryResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

@Service
public class OverpassProvider implements NearestLocationProvider {

  private static final Logger LOGGER = LogManager.getLogger(OverpassProvider.class);

  @Override
  @HystrixCommand(fallbackMethod = "overpassNotAvailable")
  public List<CompletableFuture<NearestLocationProviderResponse>> get(QueryData queryData) {
    LOGGER.info("Querying for: " + StringUtils.join(queryData.getLocationTypes().toString(), ", "));
    String query = prepareQuery(queryData.getLongitude(), queryData.getLatitude(), queryData.getLocationTypes());
    try {
      Response<OverpassQueryResult> response = asyncQuery(query);
      LOGGER.info(response.raw());
      NearestLocationProviderResponse result = mapResponse(response);

      return Collections.singletonList(CompletableFuture.completedFuture(result));
    } catch (IOException e) {
      LOGGER.warn("There was a problem when queering Overpass API: " + e);
      throw new ProviderException("There was a problem with Overpass API: " + e);
    }
  }

  @Async
  private Response<OverpassQueryResult> asyncQuery(String query) throws IOException {
    return OverpassServiceProvider.get().interpreter(query).execute();
  }

  private NearestLocationProviderResponse mapResponse(Response<OverpassQueryResult> response) throws IOException {
    if (response.isSuccessful()) {
      return new NearestLocationProviderResponse()
          .withLocations(response.body().getElements()
              .stream()
              .map(this::getLocation)
              .collect(toList()));
    } else {
      LOGGER.warn(response.errorBody().string());
      throw new ProviderException("There was a problem with Overpass API");
    }
  }

  private static String prepareQuery(double longitude, double latitude, List<LocationType> locationType) {
    return new QueryBuilder()
        .withLongitude(longitude)
        .withLatitude(latitude)
        .withLocationType(locationType)
        .build();
  }

  private Location getLocation(Element element) {
    return new Location()
        .withName(element.getTags().getName())
        .withAddress(address(element))
        .withLocationType(LocationType.byOverpassQueryCode(element.getTags().getAmenity()));
  }

  private Address address(Element element) {
    return new Address()
        .withLatitude(element.getLat())
        .withLongitude(element.getLon());
  }

  @SuppressWarnings("unused")
  public List<CompletableFuture<NearestLocationProviderResponse>> overpassNotAvailable(QueryData queryData) {
    LOGGER.warn("Overpass provider failed. Using hystrix backup response");
    return Collections.singletonList(CompletableFuture.completedFuture(new NearestLocationProviderResponse()
        .withSuccessful(false)));
  }
}

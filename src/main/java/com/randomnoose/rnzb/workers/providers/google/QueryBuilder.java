package com.randomnoose.rnzb.workers.providers.google;

import com.randomnoose.rnzb.workers.model.LocationType;
import org.apache.commons.lang3.StringUtils;

public class QueryBuilder {

  private static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
  private static final int DEFAULT_RADIUS_METERS = 1000;

  private String apiKey;
  private double longitude;
  private boolean isLongitudeSet;
  private double latitude;
  private boolean isLatitudeSet;
  private LocationType locationType;
  private int radiusMeters = DEFAULT_RADIUS_METERS;

  public QueryBuilder withApiKey(String apiKey) {
    this.apiKey = apiKey;
    return this;
  }

  public QueryBuilder withLongitude(double longitude) {
    this.longitude = longitude;
    this.isLongitudeSet = true;
    return this;
  }

  public QueryBuilder withLatitude(double latitude) {
    this.latitude = latitude;
    this.isLatitudeSet = true;
    return this;
  }

  public QueryBuilder withLocationTypes(LocationType locationTypes) {
    this.locationType = locationTypes;
    return this;
  }

  public QueryBuilder withRadiusMeters(int radiusMeters) {
    this.radiusMeters = radiusMeters;
    return this;
  }

  public String build() {
    if (!isLongitudeSet || !isLatitudeSet || locationType == null) {
      return StringUtils.EMPTY;
    }
    return BASE_URL + "?location=" + latitude + "," + longitude +
        "&type=" + locationType.getGoogleType() +
        "&radius=" + radiusMeters +
        "&key=" + apiKey;
  }
}

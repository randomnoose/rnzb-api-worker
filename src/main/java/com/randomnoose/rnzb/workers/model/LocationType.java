package com.randomnoose.rnzb.workers.model;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public enum LocationType {

  HOSPITAL("hospital", Constants.AMENITY, "hospital"),
  SUPERMARKET("supermarket", Constants.SHOP, "supermarket"),
  GAS_STATION("fuel", Constants.AMENITY, "gas_station"),
  HOTEL("hotel", Constants.TOURISM, "lodging"),
  CINEMA("cinema", Constants.AMENITY, "movie_theater"),
  OTHER("", "", "");

  private static final Map<String, LocationType> OVERPASS_LOOKUP = Maps.uniqueIndex(asList(values()), LocationType::getOverpassQueryCode);
  private static final Map<String, LocationType> GOOGLE_LOOKUP = Maps.uniqueIndex(asList(values()), LocationType::getGoogleType);

  private final String overpassQueryCode;
  private final String overpassQueryTag;
  private final String googleType;

  LocationType(String overpassQueryCode, String overpassQueryTag, String googleType) {
    this.overpassQueryCode = overpassQueryCode;
    this.overpassQueryTag = overpassQueryTag;
    this.googleType = googleType;
  }

  public String getOverpassQueryCode() {
    return overpassQueryCode;
  }

  public String getOverpassQueryTag() {
    return overpassQueryTag;
  }

  public String getGoogleType() {
    return googleType;
  }

  public static LocationType byOverpassQueryCode(String queryCode) {
    return OVERPASS_LOOKUP.get(queryCode);
  }

  public static LocationType byGoogleTypeCode(String googleType) {
    return GOOGLE_LOOKUP.get(googleType);
  }

  public static List<LocationType> fromString(String typesString) {
    String[] types = StringUtils.split(typesString, ",");
    List<LocationType> parsedTypes = new ArrayList<>();
    for (String type : types) {
      parsedTypes.add(LocationType.valueOf(type.toUpperCase()));
    }
    return parsedTypes;
  }

  private static class Constants {

    static final String AMENITY = "amenity";
    static final String SHOP = "shop";
    static final String TOURISM = "tourism";
  }
}

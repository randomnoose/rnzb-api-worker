package com.randomnoose.rnzb.workers.providers.google;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.randomnoose.rnzb.workers.model.Address;
import com.randomnoose.rnzb.workers.model.Location;
import com.randomnoose.rnzb.workers.model.LocationType;
import com.randomnoose.rnzb.workers.model.QueryData;
import com.randomnoose.rnzb.workers.providers.NearestLocationProvider;
import com.randomnoose.rnzb.workers.providers.NearestLocationProviderResponse;
import com.randomnoose.rnzb.workers.providers.ProviderException;
import com.randomnoose.rnzb.workers.providers.google.model.LocationData;
import com.randomnoose.rnzb.workers.providers.google.model.LocationQueryResults;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

@Service
public class GoogleMapsProvider implements NearestLocationProvider {

  private static final Logger LOGGER = LogManager.getLogger(GoogleMapsProvider.class);

  @Value("${google.api.key}")
  private String apiKey;
  private RestTemplate restTemplate;

  @Override
  @HystrixCommand(fallbackMethod = "googleNotAvailable")
  public List<CompletableFuture<NearestLocationProviderResponse>> get(QueryData queryData) {
    return queryData.getLocationTypes()
        .stream()
        .map(locationType -> getResponse(queryData, locationType))
        .collect(toList());
  }

  @Async
  private CompletableFuture<NearestLocationProviderResponse> getResponse(QueryData queryData, LocationType locationType) {
    LOGGER.info("Querying for: " + locationType);
    String query = prepareQuery(queryData, locationType);
    ResponseEntity<LocationQueryResults> response = restTemplate.getForEntity(query, LocationQueryResults.class);
    LOGGER.info(response);
    if (response.getStatusCode() != HttpStatus.OK) {
      LOGGER.warn("There was a problem when queering Google API: " + response.toString());
      throw new ProviderException("There was a problem with Google Maps API");
    }
    NearestLocationProviderResponse result = mapToLocations(response.getBody());
    return CompletableFuture.completedFuture(result);
  }

  private String prepareQuery(QueryData queryData, LocationType locationType) {
    return new QueryBuilder()
        .withApiKey(apiKey)
        .withLongitude(queryData.getLongitude())
        .withLatitude(queryData.getLatitude())
        .withLocationTypes(locationType)
        .build();
  }

  private NearestLocationProviderResponse mapToLocations(LocationQueryResults results) {
    return new NearestLocationProviderResponse()
        .withLocations(results.getResults()
            .stream()
            .map(this::getLocation)
            .collect(toList()));
  }

  private Location getLocation(LocationData location) {
    return new Location()
        .withName(location.getName())
        .withAddress(address(location))
        .withLocationType(locationType(location));
  }

  private Address address(LocationData location) {
    return new Address()
        .withLatitude(location.getGeometry().getLocation().getLatitude())
        .withLongitude(location.getGeometry().getLocation().getLongitude());
  }

  private LocationType locationType(LocationData location) {
    return location.getTypes()
        .stream()
        .map(LocationType::byGoogleTypeCode)
        .filter(Objects::nonNull)
        .findAny()
        .orElse(LocationType.OTHER);
  }

  public List<CompletableFuture<NearestLocationProviderResponse>> googleNotAvailable(QueryData queryData) {
    LOGGER.warn("Google provider failed. Using hystrix backup response");
    return Collections.singletonList(CompletableFuture.completedFuture(new NearestLocationProviderResponse()
        .withSuccessful(false)));
  }

  @Autowired
  public void setRestTemplate(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }
}

package com.randomnoose.rnzb.workers.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
public class NearestLocationsResponse {

  private List<Locations> locationsTypes;
  private List<String> errorMessages;

  public List<Locations> getLocationsTypes() {
    return locationsTypes;
  }

  public NearestLocationsResponse withLocationsTypes(List<Locations> locationsTypes) {
    this.locationsTypes = locationsTypes;
    return this;
  }

  public List<String> getErrorMessages() {
    return errorMessages;
  }

  public NearestLocationsResponse withErrorMessages(List<String> errorMessages) {
    this.errorMessages = errorMessages;
    return this;
  }

  public NearestLocationsResponse withErrorMessages(Errors errors) {
    ArrayList<String> errorMessagesFromErrors = new ArrayList<>();
    errorMessagesFromErrors.add("Validation failed. " + errors.getErrorCount() + " error(s)");
    for (ObjectError objectError : errors.getAllErrors()) {
      errorMessagesFromErrors.add(objectError.getDefaultMessage());
    }
    this.errorMessages = errorMessagesFromErrors;
    return this;
  }

}

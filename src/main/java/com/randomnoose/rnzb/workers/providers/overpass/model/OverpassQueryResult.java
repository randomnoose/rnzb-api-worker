package com.randomnoose.rnzb.workers.providers.overpass.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OverpassQueryResult {

  @SerializedName("elements")
  private List<Element> elements = new ArrayList<>();

  public List<Element> getElements() {
    return elements;
  }

  public OverpassQueryResult withElements(List<Element> elements) {
    this.elements = elements;
    return this;
  }
}

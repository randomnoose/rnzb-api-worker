package com.randomnoose.rnzb.workers.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.Map;

import static java.util.Collections.emptyMap;

@JsonInclude(Include.NON_NULL)
public class Location {

  private String name;
  private Address address;
  private LocationType locationType;
  private Map<String, String> otherTags = emptyMap();

  public String getName() {
    return name;
  }

  public Location withName(String name) {
    this.name = name;
    return this;
  }

  public Address getAddress() {
    return address;
  }

  public Location withAddress(Address address) {
    this.address = address;
    return this;
  }

  public LocationType getLocationType() {
    return locationType;
  }

  public Location withLocationType(LocationType locationType) {
    this.locationType = locationType;
    return this;
  }

  public Map<String, String> getOtherTags() {
    return otherTags;
  }

  public Location withOtherTags(Map<String, String> otherTags) {
    this.otherTags = otherTags;
    return this;
  }
}

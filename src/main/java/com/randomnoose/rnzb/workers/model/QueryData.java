package com.randomnoose.rnzb.workers.model;

import com.google.common.collect.ImmutableList;

import java.util.List;

public class QueryData {

  private double latitude;
  private double longitude;
  private List<LocationType> locationTypes;

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public List<LocationType> getLocationTypes() {
    return locationTypes;
  }

  public static QueryData of(double latitude, double longitude, List<LocationType> locationTypes) {
    QueryData queryData = new QueryData();
    queryData.longitude = longitude;
    queryData.latitude = latitude;
    queryData.locationTypes = ImmutableList.copyOf(locationTypes);
    return queryData;
  }
}

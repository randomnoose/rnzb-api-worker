package com.randomnoose.rnzb.workers.providers.google.model;

public class Geometry {

  private Location location;

  public Location getLocation() {
    return location;
  }

  public Geometry withLocation(Location location) {
    this.location = location;
    return this;
  }
}

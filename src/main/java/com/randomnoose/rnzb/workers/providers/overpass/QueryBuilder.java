package com.randomnoose.rnzb.workers.providers.overpass;

import com.randomnoose.rnzb.workers.model.LocationType;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class QueryBuilder {

  private static final double LATITUDE_SEARCH_BOX_OFFSET = 0.015;
  private static final double LONGITUDE_SEARCH_BOX_OFFSET = 0.015;

  private List<LocationType> locationType = Collections.emptyList();
  private double latitude;
  private boolean isLatitudeSet;
  private double longitude;
  private boolean isLongitudeSet;

  public QueryBuilder withLocationType(List<LocationType> locationType) {
    this.locationType = locationType;
    return this;
  }

  public QueryBuilder withLatitude(double latitude) {
    this.latitude = latitude;
    this.isLatitudeSet = true;
    return this;
  }

  public QueryBuilder withLongitude(double longitude) {
    this.longitude = longitude;
    this.isLongitudeSet = true;
    return this;
  }

  public String build() {
    if (!isLongitudeSet || !isLatitudeSet || locationType.isEmpty()) {
      return "";
    }
    return "[out:json][timeout:30];(" +
        getSearchCriteriaString() +
        "<;); " +
        "out body center qt 100;";
  }

  private String getSearchCriteriaString() {
    return locationType.stream()
        .map(type -> getAmenityCriteriaForLocation(type) + getSearchBoxCoordinatesString(longitude, latitude))
        .collect(Collectors.joining());
  }

  private static String getAmenityCriteriaForLocation(LocationType locationType) {
    return "node[\"" + locationType.getOverpassQueryTag() + "\"=\"" + locationType.getOverpassQueryCode() + "\"]";
  }

  private static String getSearchBoxCoordinatesString(double longitude, double latitude) {
    return "(" + (latitude - LATITUDE_SEARCH_BOX_OFFSET) + "," + (longitude - LONGITUDE_SEARCH_BOX_OFFSET) + "," +
        (latitude + LATITUDE_SEARCH_BOX_OFFSET) + "," + (longitude + LONGITUDE_SEARCH_BOX_OFFSET) + ");";
  }
}

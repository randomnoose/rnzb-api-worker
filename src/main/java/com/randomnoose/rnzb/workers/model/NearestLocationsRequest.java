package com.randomnoose.rnzb.workers.model;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class NearestLocationsRequest {

  @NotBlank(message = "Longitude must not be blank")
  private String longitude;
  @NotBlank(message = "Latitude must not be blank")
  private String latitude;
  @NotEmpty(message = "There must be at least one valid location type")
  private List<LocationType> locationTypes;

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public List<LocationType> getLocationTypes() {
    return locationTypes;
  }

  public void setLocationTypes(List<LocationType> locationTypes) {
    this.locationTypes = locationTypes;
  }
}

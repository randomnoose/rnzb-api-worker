package com.randomnoose.rnzb.workers.model;

public class Address {

  private double latitude;
  private double longitude;

  public double getLatitude() {
    return latitude;
  }

  public Address withLatitude(double latitude) {
    this.latitude = latitude;
    return this;
  }

  public double getLongitude() {
    return longitude;
  }

  public Address withLongitude(double longitude) {
    this.longitude = longitude;
    return this;
  }
}

package com.randomnoose.rnzb.workers.providers;

import com.randomnoose.rnzb.workers.model.QueryData;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface NearestLocationProvider {

  List<CompletableFuture<NearestLocationProviderResponse>> get(QueryData queryData);
}

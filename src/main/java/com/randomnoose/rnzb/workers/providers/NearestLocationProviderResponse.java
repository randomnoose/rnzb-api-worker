package com.randomnoose.rnzb.workers.providers;

import com.randomnoose.rnzb.workers.model.Location;

import java.util.List;

public class NearestLocationProviderResponse {

  private List<Location> locations;
  private boolean isSuccessful;

  public List<Location> getLocations() {
    return locations;
  }

  public NearestLocationProviderResponse withLocations(List<Location> locations) {
    this.locations = locations;
    this.isSuccessful = true;
    return this;
  }

  public boolean isSuccessful() {
    return isSuccessful;
  }

  public NearestLocationProviderResponse withSuccessful(boolean successful) {
    isSuccessful = successful;
    return this;
  }
}

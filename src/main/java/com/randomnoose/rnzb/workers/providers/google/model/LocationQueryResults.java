package com.randomnoose.rnzb.workers.providers.google.model;

import java.util.List;

public class LocationQueryResults {

  private List<LocationData> results;

  public List<LocationData> getResults() {
    return results;
  }

  public LocationQueryResults withResults(List<LocationData> results) {
    this.results = results;
    return this;
  }
}

package com.randomnoose.rnzb.workers.providers.overpass.model;

import com.google.gson.annotations.SerializedName;

public class Element {

  @SerializedName("type")
  private String type;

  @SerializedName("id")
  private long id;

  @SerializedName("lat")
  private double lat;

  @SerializedName("lon")
  private double lon;

  @SerializedName("tags")
  private Tags tags = new Tags();

  public String getType() {
    return type;
  }

  public Element withType(String type) {
    this.type = type;
    return this;
  }

  public long getId() {
    return id;
  }

  public Element withId(long id) {
    this.id = id;
    return this;
  }

  public double getLat() {
    return lat;
  }

  public Element withLat(double lat) {
    this.lat = lat;
    return this;
  }

  public double getLon() {
    return lon;
  }

  public Element withLon(double lon) {
    this.lon = lon;
    return this;
  }

  public Tags getTags() {
    return tags;
  }

  public Element withTags(Tags tags) {
    this.tags = tags;
    return this;
  }
}

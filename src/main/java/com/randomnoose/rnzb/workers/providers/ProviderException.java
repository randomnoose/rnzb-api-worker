package com.randomnoose.rnzb.workers.providers;

public class ProviderException extends RuntimeException {

  public ProviderException(String message) {
    super(message);
  }

  public ProviderException(String message, Throwable cause) {
    super(message, cause);
  }
}

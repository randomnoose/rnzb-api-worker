package com.randomnoose.rnzb.workers.providers.google.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

  @JsonProperty("lat")
  private double latitude;
  @JsonProperty("lng")
  private double longitude;

  public double getLatitude() {
    return latitude;
  }

  public Location withLatitude(double latitude) {
    this.latitude = latitude;
    return this;
  }

  public double getLongitude() {
    return longitude;
  }

  public Location withLongitude(double longitude) {
    this.longitude = longitude;
    return this;
  }
}

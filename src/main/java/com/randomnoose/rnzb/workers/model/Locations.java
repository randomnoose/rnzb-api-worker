package com.randomnoose.rnzb.workers.model;

import java.util.List;

public class Locations {

  private LocationType type;
  private List<Location> locations;

  public LocationType getType() {
    return type;
  }

  public Locations withType(LocationType type) {
    this.type = type;
    return this;
  }

  public List<Location> getLocations() {
    return locations;
  }

  public Locations withLocations(List<Location> locations) {
    this.locations = locations;
    return this;
  }
}

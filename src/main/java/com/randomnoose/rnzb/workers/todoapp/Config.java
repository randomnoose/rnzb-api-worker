package com.randomnoose.rnzb.workers.todoapp;

import com.randomnoose.rnzb.workers.providers.NearestLocationProvider;
import com.randomnoose.rnzb.workers.providers.google.GoogleMapsProvider;
import com.randomnoose.rnzb.workers.providers.overpass.OverpassProvider;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Config {

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    return restTemplateBuilder.build();
  }

  @Bean(name = "overpassProvider")
  public NearestLocationProvider overpassProvider() {
    return new OverpassProvider();
  }

  @Bean(name = "googleMapsProvider")
  public NearestLocationProvider googleMapsProvider() {
    return new GoogleMapsProvider();
  }
}

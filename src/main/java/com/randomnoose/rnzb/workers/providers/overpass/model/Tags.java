package com.randomnoose.rnzb.workers.providers.overpass.model;

import com.google.gson.annotations.SerializedName;

public class Tags {

  @SerializedName("type")
  private String type;

  @SerializedName("amenity")
  private String amenity;

  @SerializedName("name")
  private String name;

  @SerializedName("phone")
  private String phone;

  @SerializedName("contact:email")
  private String contactEmail;

  @SerializedName("website")
  private String website;

  @SerializedName("addr:city")
  private String addressCity;

  @SerializedName("addr:postcode")
  private String addressPostCode;

  @SerializedName("addr:street")
  private String addressStreet;

  @SerializedName("addr:housenumber")
  private String addressHouseNumber;

  @SerializedName("wheelchair")
  private String wheelchair;

  @SerializedName("wheelchair:description")
  private String wheelchairDescription;

  @SerializedName("opening_hours")
  private String openingHours;

  @SerializedName("internet_access")
  private String internetAccess;

  @SerializedName("fee")
  private String fee;

  @SerializedName("operator")
  private String operator;

  public String getType() {
    return type;
  }

  public Tags withType(String type) {
    this.type = type;
    return this;
  }

  public String getAmenity() {
    return amenity;
  }

  public Tags withAmenity(String amenity) {
    this.amenity = amenity;
    return this;
  }

  public String getName() {
    return name;
  }

  public Tags withName(String name) {
    this.name = name;
    return this;
  }

  public String getPhone() {
    return phone;
  }

  public Tags withPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public String getContactEmail() {
    return contactEmail;
  }

  public Tags withContactEmail(String contactEmail) {
    this.contactEmail = contactEmail;
    return this;
  }

  public String getWebsite() {
    return website;
  }

  public Tags withWebsite(String website) {
    this.website = website;
    return this;
  }

  public String getAddressCity() {
    return addressCity;
  }

  public Tags withAddressCity(String addressCity) {
    this.addressCity = addressCity;
    return this;
  }

  public String getAddressPostCode() {
    return addressPostCode;
  }

  public Tags withAddressPostCode(String addressPostCode) {
    this.addressPostCode = addressPostCode;
    return this;
  }

  public String getAddressStreet() {
    return addressStreet;
  }

  public Tags withAddressStreet(String addressStreet) {
    this.addressStreet = addressStreet;
    return this;
  }

  public String getAddressHouseNumber() {
    return addressHouseNumber;
  }

  public Tags withAddressHouseNumber(String addressHouseNumber) {
    this.addressHouseNumber = addressHouseNumber;
    return this;
  }

  public String getWheelchair() {
    return wheelchair;
  }

  public Tags withWheelchair(String wheelchair) {
    this.wheelchair = wheelchair;
    return this;
  }

  public String getWheelchairDescription() {
    return wheelchairDescription;
  }

  public Tags withWheelchairDescription(String wheelchairDescription) {
    this.wheelchairDescription = wheelchairDescription;
    return this;
  }

  public String getOpeningHours() {
    return openingHours;
  }

  public Tags withOpeningHours(String openingHours) {
    this.openingHours = openingHours;
    return this;
  }

  public String getInternetAccess() {
    return internetAccess;
  }

  public Tags withInternetAccess(String internetAccess) {
    this.internetAccess = internetAccess;
    return this;
  }

  public String getFee() {
    return fee;
  }

  public Tags withFee(String fee) {
    this.fee = fee;
    return this;
  }

  public String getOperator() {
    return operator;
  }

  public Tags withOperator(String operator) {
    this.operator = operator;
    return this;
  }
}

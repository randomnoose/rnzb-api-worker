package com.randomnoose.rnzb.workers.providers.google.model;

import java.util.List;

public class LocationData {

  private Geometry geometry;
  private String name;
  private String vicinity;
  private List<String> types;

  public Geometry getGeometry() {
    return geometry;
  }

  public LocationData withGeometry(Geometry geometry) {
    this.geometry = geometry;
    return this;
  }

  public String getName() {
    return name;
  }

  public LocationData withName(String name) {
    this.name = name;
    return this;
  }

  public String getVicinity() {
    return vicinity;
  }

  public LocationData withVicinity(String vicinity) {
    this.vicinity = vicinity;
    return this;
  }

  public List<String> getTypes() {
    return types;
  }

  public LocationData withTypes(List<String> types) {
    this.types = types;
    return this;
  }
}
